function fetchUrl(url, callback)
{
    print("fetching " + url)
    var job = plasmoid.getUrl(url)
    var output = new String

    function recv(job, data) {
        if (data.length) {
            output += (data.toUtf8())
        }
    }

    function fini(job) {
        //print("ifini!!" + job.output + job.callback)
        if (callback) {
            callback(output)
        }
    }

    if (job) {
        job.data.connect(recv)
        job.finished.connect(fini)
    }
}

UrlFetcher = function()
{
    this.fetch = fetchUrl
}

