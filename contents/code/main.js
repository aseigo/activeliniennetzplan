plasmoid.include("fetchUrl.js")

var fetcher = new UrlFetcher
var svg = new PlasmaSvg('routes')
var lines = Array("active", "quick", "contour", "os", "vendor", "hauptbahnhof")
var active = new Array

function setActive(data)
{
    active = data.split("\n")
    plasmoid.update()
}

plasmoid.paintInterface = function(painter, options, rect)
{
    rect = plasmoid.rect
    rect.y = rect.y + newsLabel.geometry.bottom
    painter.translate(rect.x, rect.y)
    svg.resize(rect.width, rect.height)
    for (i in lines) {
        svg.paint(painter, svg.elementRect(lines[i]), lines[i])
    }

    for (i in active) {
        if (svg.hasElement(active[i])) {
            svg.paint(painter, svg.elementRect(active[i]), active[i])
        }
    }
}

var layout = new LinearLayout
layout.orientation = QtVertical

var newsLabel = new Label
newsLabel.text = i18n("Loading news..")
newsLabel.linkActivated.connect(plasmoid.openUrl)
newsLabel.alignment = QtAlignHCenter | QtAlignBottom
layout.addItem(newsLabel)
layout.addStretch(10)

var currentNewsItem = 0
var news = Array()
var newsSwitchTimer = new QTimer
newsSwitchTimer.singleShot = true
newsSwitchTimer.interval = 30 * 1000
newsSwitchTimer.timeout.connect(showNextNews)

function showNextNews()
{
    if (!news) {
        return;
    }

    ++currentNewsItem;
    if (!news[currentNewsItem]) {
        currentNewsItem = 0;
    }

    newsLabel.text = '<a href="' + news[currentNewsItem]['link'] + '">' + news[currentNewsItem]['title'] + '</a>';
}

fetcher.fetch("http://plasma.kde.org/active/progress.txt", setActive)
dataEngine("rss").connectSource("http://plasma.kde.org/active/news.xml", 
        function(source, data)
        {
            news = data['items'];
            newsSwitchTimer.stop()
            if (news) {
                showNextNews()
                if (news[1]) {
                    newsSwitchTimer.start()
                }
            }
        }, 30 * 60 * 1000)

